import { AsyncPipe, JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { ExampleService } from './example.service';
import {
  map,
  forkJoin,
  withLatestFrom,
  switchMap,
  tap,
  filter,
  of,
  delay,
  debounceTime,
  distinctUntilChanged,
  startWith,
  combineLatest,
  merge,
  timer,
  Observable,
  Subject,
  interval,
  mergeMap,
  take,
  concatMap,
} from 'rxjs';
import {
  ReactiveFormsModule,
  FormGroup,
  NonNullableFormBuilder,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

// https://rxjs.dev/
// https://rxmarbles.com/
// https://rxjs.dev/operator-decision-tree

@Component({
  standalone: true,
  imports: [AsyncPipe, ReactiveFormsModule, JsonPipe],
  template: `
    My Value is: {{ dataSink$ | async }}

    <form [formGroup]="form">
      <input placeholder="num1" formControlName="num1" />
      <input placeholder="num2" formControlName="num2" />
    </form>

    <div>sink2: {{ dataSink2$ | async }}</div>

    <div>sink3: {{ dataSink3$ | async }}</div>

    <div>sink4: {{ dataSink4$ | async | json }}</div>

    <div>sink5: {{ dataSink5$ | async }}</div>

    <div>sink6: {{ dataSink6$ | async }}</div>

    <div>sink7: {{ dataSink7$ | async }}</div>
  `,
})
export default class AppComponent {
  form = inject(NonNullableFormBuilder).group({
    num1: [0],
    num2: [0],
  });

  public dataSink$ = this.form.controls.num1.valueChanges.pipe(
    debounceTime(1000),
    distinctUntilChanged(),
    startWith(2),
    withLatestFrom(this.form.controls.num2.valueChanges.pipe(startWith(1))),
    map(([num1, num2]) => {
      this.num = num1 + num2;
      return [num1, num2];
    }),
    // tap(console.log),
    filter(([num1, num2]) => num1 !== num2),
    // tap(console.log),
    map(([num1, num2]) => num1 * num2)
    // delay(3000)
  );

  dataSink2$ = combineLatest([
    this.form.controls.num1.valueChanges,
    this.form.controls.num2.valueChanges,
  ]);

  dataSink3$ = merge(
    this.form.controls.num1.valueChanges,
    this.form.controls.num2.valueChanges
  );

  dataSink4$ = this.form.controls.num1.valueChanges.pipe(
    switchMap((num) =>
      this.http.get(`https://jsonplaceholder.typicode.com/todos/${num}`)
    ),
    map((val) => {
      console.log(val);
      return val;
    })
  );

  dataSink5$ = this.form.controls.num2.valueChanges.pipe(
    switchMap((num) => this.simulateFirebase(num, 5000))
  );

  dataSink6$ = this.form.controls.num2.valueChanges.pipe(
    mergeMap((num) => this.simulateFirebase(num, 5000))
  );

  dataSink7$ = this.form.controls.num2.valueChanges.pipe(
    concatMap((num) => this.simulateFirebase(num, 5000))
  );

  simulateFirebase(val: any, delay: number) {
    return interval(delay).pipe(
      map((index) => val + ' ' + index),
      take(3)
    );
  }

  private exampleService = inject(ExampleService);
  private http = inject(HttpClient);

  public num = 0;

  // ok
  sink1$ = forkJoin([
    this.exampleService.source$,
    this.exampleService.another$,
  ]).pipe(map(([num1, num2]) => num1 * num2 + this.num));

  // good
  sink2$ = this.exampleService.source$.pipe(
    withLatestFrom(this.exampleService.another$),
    map(([num1, num2]) => {
      this.num = num1 + num2;
      return [num1, num2];
    }),
    filter(([num1, num2]) => num1 !== num2),
    map(([num1, num2]) => num1 * num2)
  );

  // very bad
  sink3$ = this.exampleService.source$.pipe(
    switchMap((num1) =>
      this.exampleService.another$.pipe(map((num2) => num1 * num2))
    )
  );

  constructor() {
    // explict - unsubscribe
    const sub = of(123).subscribe();
    sub.unsubscribe();

    // implicit - operator
    of(123).pipe(take(1)).subscribe();

    // and async pipe (also implicit)
  }
}
